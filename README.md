## Quota -> Deployment

### Opis pliku

1. Sekcja tworząca namespace

Sekcja oddzielona przez "---", aby uruchomić ją osobno.
Tworzy namespace o nazwie "lab4"

```
---
...
kind: Namespace
metadata:
  creationTimestamp: null
  name: lab4
...
---
```

2. Sekcja tworząca quota

- Wykorzystanie utworzonego namespace oraz ustawienie nazwy 'lab4quota':

```
...
metadata:
  creationTimestamp: null
  name: lab4quota
  namespace: lab4
...
```

- Ustawienie przydziału zasobów:

```
...
spec:
  hard:
    cpu: "1"
    memory: 1Gi
    pods: "5"
...
```

3. Sekcja tworząca Deployment

- Wykorzystanie utworzonego namespace i ustawienie nazwy na 'restrictednginx':

```
...
kind: Deployment
metadata:
  name: restrictednginx
  namespace: lab4
...
```

- Ustawienie ilości podów - replik na 3

```
...
spec:
  replicas: 3
...
```

- Ustawienie obrazu na nginx, żądania zasobów i limitów

```
...
  template:
    ...
    spec:
      containers:
      - image: nginx
        name: nginx
        resources:
          limits:
            cpu: 250m
            memory: 256Mi
          requests:
            cpu: 125m
            memory: 64Mi
...
```

### Poprawność działania

1. Stworzenie elementów
![kubectl create](/images/create.png "Kubectl Create")\

2. Opis utworzonego Quota
![describe quota](/images/describe-quota.png "Describe Quota")\

3. Opis utworzonego Deployment
![describe deployment](/images/describe-deployment.png "Describe Deployment")\

4. Wyświetlenie podów z przestrzeni nazw 'lab4'
![get pods](/images/get-pods.png "Get Pods")\

5. Opis wybranego pod'a
![describe pod](/images/describe-pod.png "Describe Pod")\